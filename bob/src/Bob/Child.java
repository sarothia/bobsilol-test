/**
 */
package Bob;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Child</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Bob.BobPackage#getChild()
 * @model
 * @generated
 */
public interface Child extends EObject {
} // Child
